using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ajedrez;
namespace UnitTestProject3
{
    [TestClass]
    public class pruebasTablero
    {
        [TestMethod]
        public void testTamaņoDeTablero()
        {
            Pieza[,] arrayDe8Piezas = new Pieza[8, 8];

            Tablero tableroPrueba = new Tablero();

            int arrayDe8PiezasLenght1 = arrayDe8Piezas.GetLength(0);
            int arrayDe8PiezasLenght2 = arrayDe8Piezas.GetLength(1);

            int arrayTableroPrueba1 = tableroPrueba.Tablero_a.GetLength(0);
            int arrayTableroPrueba2 = tableroPrueba.Tablero_a.GetLength(1);

            Assert.AreEqual(arrayDe8PiezasLenght1, arrayTableroPrueba1);
            Assert.AreEqual(arrayDe8PiezasLenght2, arrayTableroPrueba2);

        }
    }
}
