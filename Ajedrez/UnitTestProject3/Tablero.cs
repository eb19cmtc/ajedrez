﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ajedrez
{
    class Tablero
    {
        private Pieza[,] tablero_a = new Pieza[8, 8];
        public Tablero() 
        {

            llenar();
            llenarPeones();
        }

        public void llenar()
        {
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    tablero_a[i, j] = new Vacio();
                }
            }
        }
        public void llenarPeones()
        {
            for (int l = 0; l < 8; l++)
            {
                tablero_a[1, l] = new Peon();
                tablero_a[1, l].Color = "negro";
            }
            for (int f = 0; f < 8; f++)
            {
                tablero_a[6, f] = new Peon();
                tablero_a[6, f].Color = "blanco";
            }
        }

        public Pieza[,] Tablero_a
        {
            get { return tablero_a; }
        }
    }
}
