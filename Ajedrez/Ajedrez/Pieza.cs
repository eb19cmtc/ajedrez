﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ajedrez
{
    abstract class Pieza
    {
        protected string color;
        protected bool capturado;

        public Pieza()
        {
            capturado = false;
        }

        public void piezaCapturada()
        {
            capturado = true;
        }

        public string Color
        {
            get 
            { 
                return color; 
            }
            set
            {
                color = value;
            }
        }

        protected string tipo;
        public string Tipo
        {
            get { return tipo; }
        }
    }
}
