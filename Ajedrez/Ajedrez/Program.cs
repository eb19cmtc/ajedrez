﻿using System;

namespace Ajedrez
{
    class Program
    {
        static void Main(string[] args)
        {
            Tablero miTablero = new Tablero();

            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    Console.Write("|");
                    Console.Write(miTablero.Tablero_a[i, j].Tipo);
                }
                Console.WriteLine();
            }
            
        }
    }
}
